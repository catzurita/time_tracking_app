import 'package:flutter/material.dart';

import '../../../constants.dart';
import '../../../widgets/app_drawer.dart';
import '../../../widgets/text.dart';
import './components/arbeitszeit_tab.dart';
import './components/pause_tab.dart';

class AddTimeTrackingScreen extends StatelessWidget {

    var scaffoldKey = GlobalKey<ScaffoldState>();
    
    @override
    Widget build(BuildContext context) {
        final _tabs = [
            Tab(
                child: text(
                    'Arbeitszeit',
                    family: 'AllertaStencil',
                    size: 17,
                    weight: FontWeight.w400
                )
            ),
            Tab(
                child: text(
                    'Pause',
                    family: 'AllertaStencil',
                    size: 17,
                    weight: FontWeight.w400
                )
            )
        ];

        final tabPages = [
            ArbeitszeitTab(),
            PauseTab()
        ];

        return DefaultTabController(
            length: 2,
            child: Scaffold(
                backgroundColor: kBackgroundColor,
                key: scaffoldKey,
                appBar: AppBar(
                    elevation: 0,
                    leading: Padding(
                      padding: const EdgeInsets.only(left: kDefaultSpacing),
                      child: GestureDetector(
                            onTap: () => Navigator.of(context).pop(),
                            child: Image.asset(
                                'assets/icons/pluss_button.png',
                                height: 16,
                                color: kPrimaryColor,
                            ),
                        ),
                    ),
                    title: Padding(
                        padding: const EdgeInsets.only(right: kDefaultSpacing),
                        child: Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                                Image.asset(
                                    'assets/images/flutter_logo.png',
                                    height: 40,
                                ),
                            ],
                        ),
                    ),
                    
                ),
                drawer: AppDrawer(),
                body: SingleChildScrollView(
                    child: Container(
                        child: Column(
                            children: [
                                SizedBox(height: kDefaultSpacing,),
                                Container(
                                    height: 60,
                                    child: TabBar(
                                        indicator: BoxDecoration(
                                            border: Border(
                                                bottom: BorderSide(
                                                    color: kUnderLineColor,
                                                    width: 3
                                                )
                                            ),
                                        ),
                                        tabs: _tabs
                                    ),
                                ),
                                SingleChildScrollView(
                                    child: Container(
                                        height: 900,
                                        child: TabBarView(
                                            children: tabPages,
                                        )
                                    ),
                                ),
                            ],
                        ),
                    ),
                )
            ),
        );
    }
}