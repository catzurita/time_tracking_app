# Time Tracking App

## About the project

The app is used to track and record the working times of all its employees with ease. Employees
can do the tracking while the office clerks can do the approval of those working times. With this
application it will be easier for the company to monitor the work of every employee and it will
eliminate the manual process as well as the paperwork itself.

---

## Setup Guide

1. From the repository page, click the button labeled as Clone.
2. In the drop down, select SSH.
3. Copy the URL for the repository.
4. On your local machine, open the bash shell and change the directory to your own working directory where you would like to clone the repository.
5. Paste the link that you copied from your repository.
6. Press enter, and the repository will be stored in your working directory.
7. in the directory, open the `pubspec.yaml` file on your flutter folder.
8. Save the file, this will automatically run the flutter pub get command and will install the packages.

## Flutter Version

The flutter version used to make the project is Flutter 2.2.3 with the Dart Version 2.13.4.

## Packages

[provider ^6.0.0](https://pub.dev/packages/provider)

- Used to handle the state and global data for the app drawer

[font_awesome_flutter ^9.1.0](https://pub.dev/packages/font_awesome_flutter)

- The font awesome icons was used.

[timetable ^1.0.0-alpha.4](https://pub.dev/packages/timetable/versions/1.0.0-alpha.5)

- Used to create the calendar

## Screenshots of the UI

1. Login Screen

![SignInScreen.JPG](Time%20Tracking%20App%204e03dd30bc044dedb1fd408c572b59a7/SignInScreen.jpg)

---

1. App Drawer

![Drawer.JPG](Time%20Tracking%20App%204e03dd30bc044dedb1fd408c572b59a7/Drawer.jpg)

1. Mein Konto

    a.

![MeinKonto1.JPG](Time%20Tracking%20App%204e03dd30bc044dedb1fd408c572b59a7/MeinKonto1.jpg)

b.

![MeinKonto2.JPG](Time%20Tracking%20App%204e03dd30bc044dedb1fd408c572b59a7/MeinKonto2.jpg)

c.

![MeinKonto3.JPG](Time%20Tracking%20App%204e03dd30bc044dedb1fd408c572b59a7/MeinKonto3.jpg)

1. Visitenkarte
    1. Meine Visitenkarte -  1

    ![Visitenkarte1.JPG](Time%20Tracking%20App%204e03dd30bc044dedb1fd408c572b59a7/Visitenkarte1.jpg)

    b. Meine Visitenkarte -  2

![Visitenkarte2.JPG](Time%20Tracking%20App%204e03dd30bc044dedb1fd408c572b59a7/Visitenkarte2.jpg)

c. Vorgesetzte Tab

![Visitenkarte3.JPG](Time%20Tracking%20App%204e03dd30bc044dedb1fd408c572b59a7/Visitenkarte3.jpg)

1. Zeiterfassung
    1. Time table

        ![TimeTableSscreen.JPG](Time%20Tracking%20App%204e03dd30bc044dedb1fd408c572b59a7/TimeTableSscreen.jpg)

    2. Calendar Screen - Will be navigated when the calendar icon is clicked from the Zeiterfassung screen

        ![CalendarScreen.JPG](Time%20Tracking%20App%204e03dd30bc044dedb1fd408c572b59a7/CalendarScreen.jpg)

        c. Arbeitszeit tab - Navigated when the plus icon is clicked in the Zeiterfassung screen

        ![Addtime1.JPG](Time%20Tracking%20App%204e03dd30bc044dedb1fd408c572b59a7/Addtime1.jpg)

        d. 

        ![Addtime2.JPG](Time%20Tracking%20App%204e03dd30bc044dedb1fd408c572b59a7/Addtime2.jpg)

        e. Pause Screen - Navigated if the plus icon of the Pause card is clicked 

        ![PauseScreen.JPG](Time%20Tracking%20App%204e03dd30bc044dedb1fd408c572b59a7/PauseScreen.jpg)

        f. Bereitschaftszeit Screen - Navigated if the plus icon of the Bereitschaftszeit card is clicked 

        ![BereitschaftszeitScreen.JPG](Time%20Tracking%20App%204e03dd30bc044dedb1fd408c572b59a7/BereitschaftszeitScreen.jpg)

        g. Wartezeit Screen - Navigated if the plus icon of the Wartezeit card is clicked 

        ![WartezeitScreen.JPG](Time%20Tracking%20App%204e03dd30bc044dedb1fd408c572b59a7/WartezeitScreen.jpg)

10. Pause Tab 

![PauseTab.JPG](Time%20Tracking%20App%204e03dd30bc044dedb1fd408c572b59a7/PauseTab.jpg)