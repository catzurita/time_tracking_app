import 'package:flutter/material.dart';

const kPrimaryColor = Colors.black;
const kSecondaryColor = Color(0xFFE0E0E0) ;
const kUnderLineColor = Color(0xFF8465FF);
const kBackgroundColor = Colors.white;
const kWebsiteColor = Color(0xFF00A4EA);
const kDefaultSpacing = 16.0;
