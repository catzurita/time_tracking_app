import 'package:flutter/material.dart';
import 'package:time_tracking_app/widgets/text.dart';

import './ansprechpartner.dart';
import './report_card.dart';
import '../../../constants.dart';
import '../../../widgets/text.dart';
import '../../../widgets/person_card.dart';

class MeinKontoContainer extends StatelessWidget {

    @override
    Widget build(BuildContext context) {
        return Container(
            width: double.infinity,
            height: 575,
            margin: EdgeInsets.only(
                left: kDefaultSpacing,
                right: kDefaultSpacing,
                top: kDefaultSpacing,
                bottom: kDefaultSpacing/2
            ),
            padding: EdgeInsets.all(kDefaultSpacing),
            color: kBackgroundColor,
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                    text(
                        'Mein Konto',
                        size: 22,
                        family: "AllertaStencil",
                        weight: FontWeight.w400
                    ),
                    Container(
                        padding: EdgeInsets.only(top: 10),
                        width: double.infinity,
                        child: personCard(
                            name: 'Greg Neu', 
                            email: 'max.mustermann@bkl.de', 
                            position: 'Monteur', 
                            image: 'assets/images/prof-pic.png'
                        )
                    ),
                    text(
                        'Ansprechpartner',
                        size: 22,
                        family: "AllertaStencil",
                        weight: FontWeight.w400
                    ),
                    Ansprechpartner(),
                    SizedBox(height: 35,),
                    text(
                        'Wochenbericht',
                        size: 22,
                        family: "AllertaStencil",
                        weight: FontWeight.w400
                    ),
                    Report(
                        date: '12.03 - 19.03.2021',
                        buttonText: "Wochenbericht zuschicken",
                        buttonWidth: 231,
                        buttonFunction: (){},
                    ),
                    SizedBox(height: 20,),
                    text(
                        'Monatsbericht',
                        size: 22,
                        family: "AllertaStencil",
                        weight: FontWeight.w400
                    ),
                    Report(
                        date: 'April 2020',
                        buttonText: "Monatsbericht erstellen",
                        buttonWidth: 215,
                        buttonFunction: (){},
                    )
                ],
            ),
        );
    }
}



