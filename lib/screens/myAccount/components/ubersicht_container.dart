import 'package:flutter/material.dart';

import '../../../constants.dart';
import '../../../widgets/text.dart';
import '../../../widgets/black_button.dart';

class UbersichtContainer extends StatelessWidget {

    Widget overviewData(String type, int value){
        return Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
                text(
                    type,
                    family: 'Mulish',
                    size: 14,
                    weight: FontWeight.w600
                ),
                text(
                    value.toString(),
                    family: 'AllertaStencil',
                    size: 16,
                    weight: FontWeight.w400
                ),
            ]
        );
    }

    @override
    Widget build(BuildContext context) {
        return Container(
            width: double.infinity,
            height: 385,
            margin: EdgeInsets.only(
                left: kDefaultSpacing,
                right: kDefaultSpacing,
                top: kDefaultSpacing/2,
                bottom: kDefaultSpacing/2
            ),
            child: Column(
                children: [
                    Container(
                        height: 290,
                        width: double.infinity,
                        padding: EdgeInsets.all(kDefaultSpacing),
                        color: kBackgroundColor,
                        child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                                text(
                                    'Übersicht 2021',
                                    size: 22,
                                    family: "AllertaStencil",
                                    weight: FontWeight.w400
                                ),
                                SizedBox(height: kDefaultSpacing*2,),
                                overviewData('Jahresurlaub', 25),
                                SizedBox(height: kDefaultSpacing,),
                                overviewData('Resturlaub EPOS', 10),
                                SizedBox(height: kDefaultSpacing,),
                                overviewData('Beantragt', 08),
                                SizedBox(height: kDefaultSpacing,),
                                Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: [
                                        Column(
                                            crossAxisAlignment: CrossAxisAlignment.start,
                                            children: [
                                                text(
                                                    'Übertrag Vorjahr',
                                                    family: 'Mulish',
                                                    size: 14,
                                                    weight: FontWeight.w600
                                                ),
                                                text(
                                                    '(gültig bis 31.03.2021)',
                                                    family: 'Mulish',
                                                    size: 12,
                                                    weight: FontWeight.w600,
                                                    color: Colors.grey[400]!
                                                ),
                                            ],
                                        ),
                                        text(
                                            01.toString(),
                                            family: 'AllertaStencil',
                                            size: 16,
                                            weight: FontWeight.w400
                                        ),
                                    ],
                                ),
                                SizedBox(height: 5),
                                blackButton(
                                    text: 'Urlaub beantragen', 
                                    icon: 'assets/icons/plus_klein.png', 
                                    height: 42,
                                    width: 180, 
                                    onPress: (){}
                                ),
                            ],
                        ),
                    ),
                    Container(
                        
                        height: 95,
                        width: double.infinity,
                        padding: EdgeInsets.all(kDefaultSpacing),
                        color: Colors.grey[200],
                        child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                                text(
                                    'Aktuelles Budget',
                                    family: 'Mulish',
                                    size: 14,
                                    weight: FontWeight.w600
                                ),
                                CircleAvatar(
                                    backgroundColor: Color(0xFFFFB72B),
                                    child: text(
                                        '7',
                                        family: 'AllertaStencil',
                                        size: 16,
                                        weight: FontWeight.w400,
                                        color: Colors.white
                                    ),
                                )
                            ],
                        ),
                    ),
                ],
            ),
        );
    }
}