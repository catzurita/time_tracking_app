import 'package:flutter/material.dart';

import '../models/navigation_item.dart';

class NavigationProvider extends ChangeNotifier{
    NavigationItem _navigationItem = NavigationItem.myAccount;

    NavigationItem get navigationItem => _navigationItem;

    void setNavigationItem(NavigationItem navigationItem){
        _navigationItem = navigationItem;

        notifyListeners();
    }
}