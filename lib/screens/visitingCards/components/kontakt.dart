import 'package:flutter/material.dart';

import '../../../widgets/text.dart';

class Kontakt extends StatelessWidget {

    final String t;
    final String f;
    final String m;
    final String e;

    Kontakt({
        required this.t,
        required this.f,
        required this.m,
        required this.e
    });

    @override
    Widget build(BuildContext context) {
        return Card(
            elevation: 0,
            child: Row(
                children: [
                    ConstrainedBox(
                        constraints: BoxConstraints(
                            minWidth: 44,
                            minHeight: 44,
                            maxWidth: 64,
                            maxHeight: 64,
                        ),
                        child: Image.asset('assets/icons/user(black).png'),
                    ),
                    SizedBox(width: 16),
                    Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                            text(
                                'T: $t',
                                size: 16,
                                family: "AllertaStencil",
                                weight: FontWeight.w400
                            ),
                            text(
                                'F: $f',
                                size: 16,
                                family: "AllertaStencil",
                                weight: FontWeight.w400
                            ),
                            text(
                                'M: $m',
                                size: 16,
                                family: "AllertaStencil",
                                weight: FontWeight.w400
                            ),
                            text(
                                'E: $e',
                                size: 16,
                                family: "AllertaStencil",
                                weight: FontWeight.w400
                            ),
                        ],
                    )
                ],
            ),
        );
    }
}