import 'package:flutter/material.dart';

import '../../../../constants.dart';
import '../../../../widgets/text.dart';

class TimeTable extends StatelessWidget {
    
    List generateTable(){
        List table = [];

        for(int i = 0; i<25; i++){
            table.add(
                    Row(
                    children: [
                        Container(
                            alignment: Alignment.center,
                            width: 54,
                            height: 108,
                            color: kSecondaryColor,
                            child: text(
                             i<10?
                            '0${i.toString()}:00':
                            '${i.toString()}:00',
                                family: 'Mulish',
                                size: 11,
                                weight: FontWeight.w600,
                            ),
                        ),
                        Container(
                            width: 4,
                            height: 108,
                            color: Colors.grey[400],
                        ),
                        Expanded(
                            child: Column(
                                children: [
                                    Container(
                                        height: 54,
                                        // color: kPrimaryColor,
                                        decoration: BoxDecoration(border: Border.all(color: Colors.grey[400]!)),
                                    ),                
                                    Container(
                                        height: 54,
                                        // color: kSecondaryColor,
                                        decoration: BoxDecoration(border: Border.all(color: Colors.grey[400]!)),
                                    ),
                                ],
                            )
                        )
                    ],
                )
            );
        }

        return table;
    }

    @override
    Widget build(BuildContext context) {
        return Column(
            children: [
                ...generateTable()
            ]
        );
    }
}