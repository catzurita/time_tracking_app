import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../constants.dart';
import './text.dart';
import '../models/navigation_item.dart';
import '../provider/navigation_provider.dart';

class AppDrawer extends StatefulWidget {

    @override
    _AppDrawerState createState() => _AppDrawerState();
}

class _AppDrawerState extends State<AppDrawer> {

    Widget itemBuilder({
        required String image, 
        required String title,
        required NavigationItem item, 
        VoidCallback? onClicked
        }){
        final provider = Provider.of<NavigationProvider>(context);
        final currentItem = provider.navigationItem;
        final isSelected = item == currentItem;
        
        return Container(
            width: 130,
            height: 82,
            child: GestureDetector(
                child: Column(
                    children: [
                        Image.asset(
                            image,
                            fit: BoxFit.cover,
                            color: (isSelected) ? kPrimaryColor : kSecondaryColor,
                        ),
                        text(
                            title,
                            size: 14,
                            family: 'Roboto',
                            weight: FontWeight.w500,
                            color: (isSelected) ? kPrimaryColor : kSecondaryColor
                        ),
                        if(isSelected) Container(
                            width: 93,
                            height: 4,
                            color: kUnderLineColor,
                        )
                    ],
                ),
                onTap: onClicked,
            ),
        );
    }

    void selectedItem(BuildContext context, NavigationItem item){
        final provider = Provider.of<NavigationProvider>(context, listen: false);
        provider.setNavigationItem(item);
        
        switch(item){
            case NavigationItem.myAccount:
                Navigator.pushReplacementNamed(
                    context, 
                    '/my-account'
                );
                break;
            case NavigationItem.visitingCards:
                Navigator.pushReplacementNamed(
                    context, 
                    '/visiting-cards'
                );
                break;
            case NavigationItem.timeTracking:
                Navigator.pushReplacementNamed(
                    context, 
                    '/time-tracking'
                );
                break;
            case NavigationItem.myStakes:
                return;
        }
    }

    @override
    Widget build(BuildContext context) {

        Widget drawerHeader  = DrawerHeader(
            child: Container(
                margin: EdgeInsets.only(
                    left: 30,
                    right: 40
                ),
                padding: EdgeInsets.only(bottom: 10),
                child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                        GestureDetector(
                            onTap: () => Navigator.of(context).pop(),
                            child: Image.asset(
                                'assets/icons/pluss_button.png',
                                height: 16,
                            ),
                        ),
                        Image.asset(
                            'assets/images/flutter_logo.png',
                            height: 32,
                        ),
                    ],
                ),
            ),
        );

        return Drawer(
            child: Container(
                width: double.infinity,
                color: kBackgroundColor,
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                        drawerHeader,
                        itemBuilder(
                            image: 'assets/icons/user(black).png',
                            title: 'Mein Konto',
                            item: NavigationItem.myAccount,
                            onClicked: () => selectedItem(
                                context, 
                                NavigationItem.myAccount
                            )
                        ),
                        itemBuilder(
                            image: 'assets/icons/visitenkarte.png',
                            title: 'Visitenkarte',
                            item: NavigationItem.visitingCards,
                            onClicked: () => selectedItem(
                                context,
                                NavigationItem.visitingCards
                            )
                        ),
                        itemBuilder(
                            image: 'assets/icons/time_tracking.png',
                            title: 'Zeiterfassung',
                            item: NavigationItem.timeTracking,
                            onClicked: () => selectedItem(
                                context,
                                NavigationItem.timeTracking
                            )
                        ),
                        itemBuilder(
                            image:'assets/icons/Union.png',
                            title: 'Meine Einsatze',
                            item: NavigationItem.myStakes
                        ),
                    ],
                ),
            ) ,
        );
    }
}
