// @dart=2.9
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:flutter/services.dart';
import 'package:time_machine/time_machine.dart';

import './constants.dart';
import './provider/navigation_provider.dart';
import './screens/loginScreen/login_screen.dart';
import './screens/myAccount/my_account_screen.dart';
import './screens/visitingCards/visiting_cards_screen.dart';
import './screens/timeTracking/time_tracking_screen.dart';
import './screens/calendarScreen/calendar_screen.dart';
import './screens/addTimeTracking/add_time_tracking_screen.dart';
import './screens/addTimeTracking/components/pause_screen.dart';
import './screens/addTimeTracking/components/bereitschaftszeit_screen.dart';
import './screens/addTimeTracking/components/wartezeit_screen.dart';

void main() async {
    
    WidgetsFlutterBinding.ensureInitialized();
    await TimeMachine.initialize({'rootBundle': rootBundle});
    runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
    @override
    Widget build(BuildContext context) {
        return ChangeNotifierProvider(
            create: (context) => NavigationProvider(),
            child: MaterialApp(
                title: 'Time Tracking App',
                debugShowCheckedModeBanner: false,
                theme: ThemeData(
                    primaryColor: kPrimaryColor,
                    scaffoldBackgroundColor: kSecondaryColor,
                    appBarTheme: AppBarTheme(
                        color: kBackgroundColor,
                    ),
            ),
                home: LoginScreen(),
                routes: {
                    '/my-account': (context) => MyAccountScreen(),
                    '/visiting-cards': (context) => VisitingCardsScreen(),
                    '/time-tracking': (context) => TimeTrackingScreen(),
                    '/calendar-screen': (context) => CalendarScreen(),
                    '/add-time-tracking': (context) => AddTimeTrackingScreen(),
                    '/pause-screen': (context) => PauseScreen(),
                    '/bereitchaftszeit-screen': (context) => BereitschaftszeitScreen(),
                    '/wartezeit-screen': (context) => WartezeitScreen()
                }
            ),
        );
    }
}
