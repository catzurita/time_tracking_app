import 'package:flutter/material.dart';

import '../../../widgets/app_drawer.dart';
import './components/mein_konto_container.dart';
import './components/ubersicht_container.dart';
import './components/krankheitstage_container.dart';
import './components/az_konto_container.dart';

class MyAccountScreen extends StatelessWidget {
    
    var scaffoldKey = GlobalKey<ScaffoldState>();
    @override
    Widget build(BuildContext context) {
        return Scaffold(
            key: scaffoldKey,
            appBar: AppBar(
                leading: IconButton(
                    onPressed: (){
                        scaffoldKey.currentState?.openDrawer();
                    },
                    icon: Image.asset(
                        'assets/icons/menu.png',
                        fit: BoxFit.cover,
                    )
                ),
            ),
            drawer: AppDrawer(),
            body: SingleChildScrollView(
                child: Column(
                    children: [
                        MeinKontoContainer(),
                        UbersichtContainer(),
                        KrankheitstageContainer(),
                        AzKontoContainer()
                    ]
                ),
            ),
        );
    }
}