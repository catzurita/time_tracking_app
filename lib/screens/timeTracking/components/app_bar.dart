import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import '../../../../constants.dart';
import '../../../../widgets/text.dart';

AppBar appBarBuild(
    GlobalKey<ScaffoldState> scaffoldKey, 
    BuildContext context
) {
        return AppBar(
            elevation: 0,
            title: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                    text(
                        'Donnerstag',
                        family: 'AllertaStencil',
                        size: 22,
                        weight: FontWeight.w400
                    ),
                    Row(
                        children: [
                            Image.asset(
                                'assets/icons/offen.png'
                            ),
                            text(
                                '12.01.2021',
                                family: 'Mulish',
                                size: 12,
                                weight: FontWeight.w600,
                                color: Colors.grey[400]!
                            ),
                        ],
                    )
                ],
            ),
            actions: [
                InkWell(
                    onTap: () => Navigator.pushNamed(
                        context, 
                        '/calendar-screen'
                    ),
                    child: CircleAvatar(
                        backgroundColor: kPrimaryColor,
                        radius: 22,
                        child: CircleAvatar(
                            backgroundColor: kBackgroundColor,
                            child: ClipRRect(
                                borderRadius: BorderRadius.circular(4),
                                child: FaIcon(
                                    FontAwesomeIcons.calendarAlt,
                                    color: kPrimaryColor,
                                )
                            ),
                        ),
                    ),
                ),
                SizedBox(width: kDefaultSpacing,), 
                InkWell(
                    onTap: () => Navigator.pushNamed(
                        context, 
                        '/add-time-tracking'
                    ),
                    child: CircleAvatar(
                        backgroundColor: kSecondaryColor,
                        radius: 21,
                        child: ClipRRect(
                            borderRadius: BorderRadius.circular(5),
                            child: FaIcon(
                                    FontAwesomeIcons.plus,
                                    color: kPrimaryColor,
                                    size: 18,
                            )
                        ),
                    ),
                ),
                SizedBox(width: kDefaultSpacing,) 
            ],
            leading: IconButton(
                onPressed: (){
                    scaffoldKey.currentState?.openDrawer();
                },
                icon: Image.asset(
                    'assets/icons/menu.png',
                    fit: BoxFit.cover,
                )
            ),
              
        );
    }