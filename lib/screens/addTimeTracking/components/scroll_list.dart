import 'package:flutter/material.dart';

import '../../../widgets/text.dart';
import '../../../constants.dart';
import '../../../widgets/list_wheel_scroll_view.dart';

class ScrollList extends StatefulWidget {
  
    @override
    _ScrollListState createState() => _ScrollListState();
}

class _ScrollListState extends State<ScrollList> {

    @override
    Widget build(BuildContext context) {
        return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
                Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                        Container(
                            padding: EdgeInsets.symmetric(vertical: kDefaultSpacing),
                            width: 165,
                            height: 145,
                            decoration: BoxDecoration(
                                border: Border(
                                    bottom: BorderSide(
                                        color: Color(0xFF4CB7E5),
                                        width: 3
                                    )
                                )
                            ),
                            child: Row(
                                children: [
                                    Container(  
                                        child: Center(
                                            child: Image.asset('assets/icons/play_klein.png'),
                                        ),
                                    ),
                                    Container(
                                        child: ListWheelScroll()
                                    )
                                ],
                            ),
                        ),
                        Container(
                            padding: EdgeInsets.symmetric(vertical: kDefaultSpacing),
                            width: 165,
                            height: 145,
                            decoration: BoxDecoration(
                                border: Border(
                                    bottom: BorderSide(
                                        color: Color(0xFF4CB7E5),
                                        width: 3
                                    )
                                )
                            ),
                            child: Row(
                                children: [
                                    Container(
                                        child: Center(
                                            child: Image.asset('assets/icons/stop_klein.png'),
                                        ),
                                    ),
                                    Container(
                                        child: ListWheelScroll()
                                    )
                                ],
                            ),
                        ),
                    ],
                )
            ],
        );
    }
}