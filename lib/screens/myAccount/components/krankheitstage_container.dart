import 'package:flutter/material.dart';

import '../../../constants.dart';
import '../../../widgets/text.dart';
import '../../../widgets/black_button.dart';

class KrankheitstageContainer extends StatelessWidget {
  
    @override
    Widget build(BuildContext context) {
        return Container(
            width: double.infinity,
            height: 180,
            margin: EdgeInsets.only(
                left: kDefaultSpacing,
                right: kDefaultSpacing,
                top: kDefaultSpacing/2,
                bottom: kDefaultSpacing/2
            ),
            color: kBackgroundColor,
            padding: EdgeInsets.all(kDefaultSpacing),
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                    text(
                        'Krankheitstage',
                        size: 22,
                        family: "AllertaStencil",
                        weight: FontWeight.w400
                    ),
                    SizedBox(height: kDefaultSpacing*2,),
                    Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                            text(
                                'Insgesamt',
                                family: 'Mulish',
                                size: 14,
                                weight: FontWeight.w600
                            ),
                            text(
                                03.toString(),
                                family: 'AllertaStencil',
                                size: 16,
                                weight: FontWeight.w400
                            )
                        ]
                    ),
                    SizedBox(height: kDefaultSpacing,),
                    blackButton(
                                text: 'Krankheit einreichen', 
                                icon: 'assets/icons/plus_klein.png', 
                                height: 42,
                                width: 191, 
                                onPress: (){}
                            ),
                ],
            ),
        );
    }
}