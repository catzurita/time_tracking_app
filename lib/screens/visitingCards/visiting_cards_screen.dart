import 'package:flutter/material.dart';

import '../../../constants.dart';
import '../../../widgets/app_drawer.dart';
import '../../../widgets/text.dart';
import './components/meine_visitenkarte_tab.dart';
import './components/vorgesetzte_tab.dart';

class VisitingCardsScreen extends StatelessWidget {

    var scaffoldKey = GlobalKey<ScaffoldState>();
    
    @override
    Widget build(BuildContext context) {
        final _tabs = [
            Tab(
                child: text(
                    'Meine Visitenkarte',
                    family: 'AllertaStencil',
                    size: 17,
                    weight: FontWeight.w400
                )
            ),
            Tab(
                child: text(
                    'Vorgesetzte',
                    family: 'AllertaStencil',
                    size: 17,
                    weight: FontWeight.w400
                )
            )
        ];

        final tabPages = [
            MeinVisitenKarteTab(),
            VorgesetzteTab()
        ];

        return DefaultTabController(
            length: 2,
            child: Scaffold(
                key: scaffoldKey,
                appBar: AppBar(
                    leading: IconButton(
                        onPressed: (){
                            scaffoldKey.currentState?.openDrawer();
                        },
                        icon: Image.asset(
                            'assets/icons/menu.png',
                            fit: BoxFit.cover,
                        )
                    ),
                ),
                drawer: AppDrawer(),
                body: SingleChildScrollView(
                    child: Container(
                        child: Column(
                            children: [
                                SizedBox(height: kDefaultSpacing,),
                                Container(
                                    height: 60,
                                    child: TabBar(
                                        indicator: BoxDecoration(
                                            border: Border(
                                                bottom: BorderSide(
                                                    color: kUnderLineColor,
                                                    width: 3
                                                )
                                            ),
                                        ),
                                        tabs: _tabs
                                    ),
                                ),
                                Container(
                                    height: 750,
                                    child: TabBarView(
                                        children: tabPages,
                                    )
                                ),
                            ],
                        ),
                    ),
                )
            ),
        );
    }
}