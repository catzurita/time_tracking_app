import 'package:flutter/material.dart';

import '../../../widgets/text.dart';

class Ansprechpartner extends StatelessWidget {
  
    @override
    Widget build(BuildContext context) {
        return Container(
            margin: EdgeInsets.only(top: 10),
            child: Card(
                elevation: 0,
                child: Row(
                    children: [
                        CircleAvatar(
                            backgroundImage: AssetImage( 'assets/images/flamingo.png'),
                            radius: 32,
                        ),
                        SizedBox(width: 16),
                        Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                                text(
                                    'Ingo Flamingo',
                                    size: 16,
                                    family: "AllertaStencil",
                                    weight: FontWeight.w400
                                ),
                                text(
                                    'info.flamingo@f-bootcamp.com',
                                    family: 'Mulish',
                                    weight: FontWeight.w600,
                                    size: 12,
                                    color: Colors.grey[400]!
                                ),
                                Image.asset('assets/icons/status.png')
                            ],
                        )
                    ],
                ),
            ),
        );
    }
}