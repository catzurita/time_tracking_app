import 'package:flutter/material.dart';

import '../../../constants.dart';
import '../../../widgets/text.dart';

class AzKontoContainer extends StatelessWidget {
 
    @override
    Widget build(BuildContext context) {
        return Container(
            width: double.infinity,
            height: 150,
            margin: EdgeInsets.only(
                left: kDefaultSpacing,
                right: kDefaultSpacing,
                top: kDefaultSpacing/2,
                bottom: kDefaultSpacing
            ),
            color: kBackgroundColor,
            padding: EdgeInsets.all(kDefaultSpacing),
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                    text(
                        'Krankheitstage',
                        size: 22,
                        family: "AllertaStencil",
                        weight: FontWeight.w400
                    ),
                    SizedBox(height: kDefaultSpacing*2,),
                    Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                            text(
                                'Stunden',
                                family: 'Mulish',
                                size: 14,
                                weight: FontWeight.w600
                            ),
                            text(
                                "100/250",
                                family: 'AllertaStencil',
                                size: 16,
                                weight: FontWeight.w400
                            )
                        ]
                    ),
                ],
            ),
        );
    }
}