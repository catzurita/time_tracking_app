import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import '../../../widgets/text.dart';
import '../../../constants.dart';

class PlusButtonCard extends StatelessWidget {

    final String title;
    final String subtitle;
    final VoidCallback onPress;
    final Color buttonColor;

    PlusButtonCard({
        required this.title,
        required this.subtitle,
        required this.onPress,
        required this.buttonColor
    });

    @override
    Widget build(BuildContext context) {
        return Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
                Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                        text(
                            title,
                            family: "AllertaStencil",
                            size: 22,
                            weight: FontWeight.w400
                        ),
                        text(
                            subtitle,
                            family: "Roboto",
                            size: 14,
                            weight: FontWeight.w400,
                            color: Colors.grey[350]!
                        ),
                    ],
                ),
                GestureDetector(
                    onTap: onPress,
                    child: CircleAvatar(
                        backgroundColor: buttonColor,
                        radius: 21,
                        child: ClipRRect(
                            borderRadius: BorderRadius.circular(5),
                            child: FaIcon(
                                    FontAwesomeIcons.plus,
                                    color: kBackgroundColor,
                                    size: 15,
                            )
                        ),
                    ),
                )
            ],
        );
    }
}