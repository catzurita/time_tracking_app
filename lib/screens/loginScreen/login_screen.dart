import 'package:flutter/material.dart';

import '../../constants.dart';
import './components/sign_in_button.dart';
import '../../../widgets/text.dart';

class LoginScreen extends StatelessWidget {
  
    @override
    Widget build(BuildContext context) {
        return Scaffold(
            backgroundColor: kBackgroundColor,
            body: SafeArea(
                child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: kDefaultSpacing),
                    child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                            SizedBox(height: 154),
                            Image.asset(
                                'assets/images/flutter_logo.png',
                                height: 75,
                            ),
                            SizedBox(height: 48,),
                            Container(
                                alignment: Alignment.center,
                                width: double.infinity,
                                margin: EdgeInsets.all(25),
                                child: text(
                                    'Flutter FieldPass',
                                    family: 'Roboto',
                                    size: 14,
                                    weight: FontWeight.bold
                                )
                            ),
                            SizedBox(height: 56,),
                            SignInButton(),
                            Spacer(),
                            Align(
                                alignment: Alignment.bottomCenter,
                                child: Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                        text(
                                            'Impressum',
                                            family: 'Mulish',
                                            size: 14,
                                            weight: FontWeight.w600
                                        ),
                                        SizedBox(width: 32,),
                                        text(
                                            'Datenschutz',
                                            family: 'Mulish',
                                            size: 14,
                                            weight: FontWeight.w600
                                        ),
                                    ],
                                ),
                            ),
                            SizedBox(height: 32,)      
                        ],
                    )
                )
            )
        );
    }
}

