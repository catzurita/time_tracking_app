import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import '../../../../constants.dart';
import '../../../../widgets/text.dart';

class DaysIcons extends StatelessWidget {
    
    Widget dayAvatarsIcons({
        required Color bgColor,
        required Color iconColor,
        required String day,
        required IconData icon
    }){
        return Column(
            children: [
                CircleAvatar(
                    radius: 21,
                    backgroundColor: bgColor,
                    child:  ClipRRect(
                        borderRadius: BorderRadius.circular(5),
                        child: FaIcon(
                                icon,
                                color: iconColor,
                                size: 15,
                        )
                    ),
                ),
                text(
                    day,
                    family: 'AllertaStencil',
                    size: 12,
                    weight: FontWeight.w400
                )

            ],
        );
    }
    Widget dayAvatarsLetters({
        required Color bgColor,
        required Color iconColor,
        required String day,
    }){
        return Column(
            children: [
                CircleAvatar(
                    radius: 21,
                    backgroundColor: bgColor,
                    child:  ClipRRect(
                        borderRadius: BorderRadius.circular(5),
                        child: text(
                            day,
                            family: 'AllertaStencil',
                            size: 21,
                            weight: FontWeight.w400,
                            color: Colors.grey[350]!
                        )
                    ),
                ),
                text(
                    day,
                    family: 'AllertaStencil',
                    size: 12,
                    weight: FontWeight.w400
                )

            ],
        );
    }
    @override
    Widget build(BuildContext context) {
        return Container(
            child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                    dayAvatarsIcons(
                        bgColor: Color(0xFFE14141), 
                        iconColor: kPrimaryColor,
                        day: 'Mo', 
                        icon: FontAwesomeIcons.plus
                    ),
                    dayAvatarsIcons(
                        bgColor: Color(0xFFFFB72B), 
                        iconColor: kPrimaryColor,
                        day: 'Di', 
                        icon: FontAwesomeIcons.plus
                    ),
                    dayAvatarsIcons(
                        bgColor: Color(0xFF6788FF), 
                        iconColor: kBackgroundColor,
                        day: 'Mi', 
                        icon: FontAwesomeIcons.solidPaperPlane
                    ),
                    dayAvatarsIcons(
                        bgColor: Color(0xFF8465FF), 
                        iconColor: kBackgroundColor,
                        day: 'Do', 
                        icon: FontAwesomeIcons.pen
                    ),
                    dayAvatarsLetters(
                        bgColor: Color(0xFFE0E0E0), 
                        iconColor: kPrimaryColor,
                        day: 'Fr', 
                    ),
                    dayAvatarsLetters(
                        bgColor: Color(0xFFE0E0E0), 
                        iconColor: kPrimaryColor,
                        day: 'Sa', 
                    ),
                    dayAvatarsLetters(
                        bgColor: Color(0xFFE0E0E0), 
                        iconColor: kPrimaryColor,
                        day: 'So', 
                    ),
                    
                ],
            ),
        );
    }
}