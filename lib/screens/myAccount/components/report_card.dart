import 'package:flutter/material.dart';

import '../../../widgets/black_button.dart';
import '../../../widgets/text.dart';

class Report extends StatelessWidget {
    
    final String date;
    final String buttonText;
    final double buttonWidth;
    final VoidCallback buttonFunction;

    Report({
        required this.date,
        required this.buttonText,
        required this.buttonWidth,
        required this.buttonFunction
    });

    @override
    Widget build(BuildContext context) {
        return Card(
            elevation: 0,
            child: Row(
                children: [
                    ConstrainedBox(
                        constraints: BoxConstraints(
                            minWidth: 44,
                            minHeight: 44,
                            maxWidth: 64,
                            maxHeight: 64,
                        ),
                        child: Image.asset('assets/icons/kalender.png'),
                    ),
                    SizedBox(width: 16),
                    Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                            text(
                                date,
                                size: 16,
                                family: "AllertaStencil",
                                weight: FontWeight.w400
                            ),
                            SizedBox(height: 16),
                            blackButton(
                                text: buttonText, 
                                icon: 'assets/icons/send_klein.png', 
                                height: 42,
                                width: buttonWidth, 
                                onPress: buttonFunction
                            ),
                        ],
                    )
                ],
            ),
        );
    }
}