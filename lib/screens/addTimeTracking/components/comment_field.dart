import 'package:flutter/material.dart';

class CommentField extends StatelessWidget {
    
    @override
    Widget build(BuildContext context) {
        return Container(
            width: double.infinity,
            height: 150,
            child: Row(
                children: [
                    Container(
                        color: Color(0xFFF5F5F5),
                        width: 70,
                        padding: EdgeInsets.only(
                            top:5,
                            left:5,
                            right:5,
                            bottom:60,
                        ),
                        height: 99,
                        child: CircleAvatar(
                                radius: 50,
                                child: Image.asset('assets/images/prof-pic.png'),
                            ),
                    ),
                    Expanded(
                        child: TextFormField(
                            maxLines: 3,
                            decoration: InputDecoration(
                                alignLabelWithHint: true,
                                border: InputBorder.none,
                                fillColor: Color(0xFFF5F5F5),
                                filled: true,
                                labelText: 'Bemerkung hinzufügen',
                                labelStyle: TextStyle(
                                    fontFamily: 'Mulish',
                                    color: Colors.grey[400],
                                    fontWeight: FontWeight.w400,
                                    fontSize: 16,
                                ),
                            )
                        ),
                    ),
                ],
            ) ,
        );
    }
}



