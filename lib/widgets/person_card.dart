import 'package:flutter/material.dart';

import './text.dart';
import '../constants.dart';

Widget personCard ({
    required String name, 
    required String email, 
    required String position, 
    required String image}){

    return Card(
        elevation: 0,
        child: Row(
            children: [
                CircleAvatar(
                    backgroundImage: AssetImage(image),
                    radius: 32,
                    backgroundColor: kBackgroundColor,
                ),
                SizedBox(width: 16),
                Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                        text(
                            '$name',
                            size: 16,
                            family: "AllertaStencil",
                            weight: FontWeight.w400
                        ),
                        text(
                            '$email',
                            family: 'Mulish',
                            weight: FontWeight.w600,
                            size: 12,
                            color: Colors.grey[400]!
                        ),
                        text(
                            '$position',
                            family: 'Mulish',
                            weight: FontWeight.w600,
                            size: 12,
                            color: Colors.grey[400]!
                        ),
                    ],
                )
            ],
        ),
    );
}