import 'package:flutter/material.dart';

import './comment_field.dart';
import './plus_button_card.dart';
import './scroll_list.dart';
import '../../../constants.dart';
import '../../../widgets/text.dart';
import '../../../widgets/black_button.dart';

class ArbeitszeitTab extends StatefulWidget {
    static const _categoryItems = [
        'Baustellenvorbereitung'
    ];

    static const _projectItems = [
        '1298721398'
    ];

    @override
    _ArbeitszeitTabState createState() => _ArbeitszeitTabState();
}

class _ArbeitszeitTabState extends State<ArbeitszeitTab> {

    final List<DropdownMenuItem<String>> _categoryItemsMap = ArbeitszeitTab._categoryItems.map(
        (String value) => DropdownMenuItem(
            value: value,
            child: text(
                value,
                family: 'Mulish',
                size: 14,
                weight: FontWeight.w600
            )
        )
    ).toList();
    final List<DropdownMenuItem<String>> _projectItemsMap = ArbeitszeitTab._projectItems.map(
        (String value) => DropdownMenuItem(
            value: value,
            child: text(
                value,
                family: 'Mulish',
                size: 14,
                weight: FontWeight.w600
            )
        )
    ).toList();

    String? _categoryButton1;
    String? _categoryButton2;

    @override
    Widget build(BuildContext context) {
        return Container(
            height: 575,
            color: kBackgroundColor,
            padding: EdgeInsets.all(kDefaultSpacing),
                    child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                            text(
                                'Kategories',
                                family: "AllertaStencil",
                                size: 22,
                                weight: FontWeight.w400
                            ),
                            Container(
                                decoration: BoxDecoration(border: 
                                    Border(
                                        bottom: BorderSide(
                                            color: kSecondaryColor,
                                        )
                                    )
                                ),
                                height: 57,
                                width: double.infinity,
                                child: Row(
                                    // mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: [
                                        Expanded(
                                            child: DropdownButton(
                                                // icon: (Image.asset('assets/icons/drop_down_arrow.png')),
                                                iconSize: 0,
                                                hint: text(
                                                    'Wahlen Sie bitte Kategorie aus',
                                                    family: 'Mulish',
                                                    size: 14,
                                                    weight: FontWeight.w600,
                                                    color: Colors.grey[350]!
                                                ),
                                                value: _categoryButton1,
                                                onChanged: (String? newValue){
                                                    setState(() {
                                                        _categoryButton1 = newValue!;
                                                    });
                                                },
                                                items: _categoryItemsMap
                                            )
                                        ),
                                        Image.asset('assets/icons/drop_down_arrow.png'),
                                        SizedBox(width: kDefaultSpacing/2,)
                                    ],
                                ),
                            ),
                            SizedBox(height: kDefaultSpacing,),
                            text(
                                'Projektnummer',
                                family: "AllertaStencil",
                                size: 22,
                                weight: FontWeight.w400
                            ),
                            Container(
                                decoration: BoxDecoration(border: 
                                    Border(
                                        bottom: BorderSide(
                                            color: kSecondaryColor,
                                        )
                                    )
                                ),
                                height: 57,
                                width: double.infinity,
                                child: Row(
                                    // mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: [
                                        Expanded(
                                            child: DropdownButton(
                                                // icon: (Image.asset('assets/icons/drop_down_arrow.png')),
                                                iconSize: 0,
                                                hint: text(
                                                    'Projektnummer hinzufügen',
                                                    family: 'Mulish',
                                                    size: 14,
                                                    weight: FontWeight.w600,
                                                    color: Colors.grey[350]!
                                                ),
                                                value: _categoryButton2,
                                                onChanged: (String? newValue){
                                                    setState(() {
                                                        _categoryButton2 = newValue!;
                                                    });
                                                },
                                                items: _projectItemsMap
                                            )
                                        ),
                                        Image.asset('assets/icons/drop_down_arrow.png'),
                                        SizedBox(width: kDefaultSpacing/2,)
                                    ],
                                ),
                            ),
                            SizedBox(height: kDefaultSpacing,),
                            PlusButtonCard(
                                title: 'Mitarbeiter', 
                                subtitle: 'hinzufügen oder bearbeiten', 
                                onPress: (){}, 
                                buttonColor: kPrimaryColor
                            ),
                            SizedBox(height: kDefaultSpacing,),
                            text(
                                'Arbeitszeit',
                                family: "AllertaStencil",
                                size: 22,
                                weight: FontWeight.w400
                            ),
                            ScrollList(),
                            SizedBox(height:kDefaultSpacing),
                            PlusButtonCard(
                                title: 'Pause', 
                                subtitle: 'hinzufügen oder bearbeiten', 
                                onPress: () => Navigator.pushNamed(
                                    context, 
                                    '/pause-screen'
                                ), 
                                buttonColor: Color(0xFF6788FF)
                            ),
                            SizedBox(height:kDefaultSpacing),
                            PlusButtonCard(
                                title: 'Wartezeit', 
                                subtitle: 'hinzufügen oder bearbeiten', 
                                onPress: () => Navigator.pushNamed(
                                    context, 
                                    '/wartezeit-screen'
                                ), 
                                buttonColor: Color(0xFFFFB72B)
                            ),
                            SizedBox(height:kDefaultSpacing),
                            PlusButtonCard(
                                title: 'Bereitschaftszeit', 
                                subtitle: 'hinzufügen oder bearbeiten', 
                                onPress: () => Navigator.pushNamed(
                                    context, 
                                    '/bereitchaftszeit-screen'
                                ), 
                                buttonColor: Color(0xFF8465FF)
                            ),
                            SizedBox(height:kDefaultSpacing),
                            CommentField(),
                            SizedBox(height:kDefaultSpacing),
                            Row(
                                mainAxisAlignment: MainAxisAlignment.end,
                                children: [
                                    text(
                                        'Abbrechen',
                                        family: 'Roboto',
                                        size: 14,
                                        weight: FontWeight.w500
                                    ),
                                    SizedBox(width: 25,),
                                    blackButton(
                                        text: 'Speichern', 
                                        icon: 'assets/icons/send_klein.png', 
                                        height: 42,
                                        width: 127, 
                                        onPress: (){}
                                    ),

                                ],
                            )
                        ],
                    ),
            );
    }
}