import 'package:black_hole_flutter/black_hole_flutter.dart';
import 'package:collection/collection.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:supercharged/supercharged.dart';
import 'package:timetable/timetable.dart';

import '../../../constants.dart';
import '../../../widgets/text.dart';

class CalendarScreen extends StatefulWidget {
    @override
    _CalendarScreenState createState() => _CalendarScreenState();
}

class _CalendarScreenState extends State<CalendarScreen> with TickerProviderStateMixin {
    
  late final _dateController = DateController(
    // All parameters are optional.
    initialDate: DateTimeTimetable.today(),
    // visibleRange: _visibleDateRange.visibleDateRange,
  );

  final _timeController = TimeController(
    // All parameters are optional.
    // minDuration: 1.hours,
    // maxDuration: 10.hours,
    // initialRange: TimeRange(8.hours, 20.hours),
    maxRange: TimeRange(0.hours, 24.hours),
  );

  final _draggedEvents = <BasicEvent>[];

    final date = DateTime.march;
  @override
  void dispose() {
    _timeController.dispose();
    _dateController.dispose();
    super.dispose();
  }

    @override
    Widget build(BuildContext context) {
        return Scaffold(
                backgroundColor: kBackgroundColor.withOpacity(0.985),
                appBar: AppBar(
                    toolbarHeight: 75,
                    title: Padding(
                        padding: EdgeInsets.only(
                            top: kDefaultSpacing,
                            bottom: kDefaultSpacing,
                        ),
                    child: text(
                        '2021',
                        family: 'AllertaStencil',
                        size: 22,
                        weight: FontWeight.w400
                    ),
                    ),
                    leading: Padding(
                    padding: const EdgeInsets.only(left: kDefaultSpacing),
                    child: GestureDetector(
                        onTap: () => Navigator.of(context).pop(),
                        child: CircleAvatar(
                            radius: 21,
                            backgroundColor: kSecondaryColor,
                            child: Image.asset(
                                'assets/icons/pluss_button.png',
                                height: 16,
                                color: kPrimaryColor,
                            ),
                        ),
                    ),
                    ),
                    
                ),
                body: TimetableConfig<BasicEvent>(
                    // Required:
                    dateController: _dateController,
                    timeController: _timeController,
                    eventBuilder: (context, event) => BasicEventWidget(event),
                    child: CompactMonthTimetable(),
                    // Optiona
                )
            );
    }
    
}