import 'package:flutter/material.dart';

import './comment_field.dart';
import './scroll_list.dart';
import '../../../constants.dart';
import '../../../widgets/text.dart';
import '../../../widgets/black_button.dart';

class PauseScreen extends StatefulWidget {
    static const _categoryItems = [
        'Baustellenvorbereitung'
    ];

    @override
    _PauseScreenState createState() => _PauseScreenState();
}

class _PauseScreenState extends State<PauseScreen> {

    final List<DropdownMenuItem<String>> _categoryItemsMap = PauseScreen._categoryItems.map(
        (String value) => DropdownMenuItem(
            value: value,
            child: text(
                value,
                family: 'Mulish',
                size: 14,
                weight: FontWeight.w600
            )
        )
    ).toList();
    
    String? _categoryButton1;
    
    @override
    Widget build(BuildContext context) {
        return Scaffold(
            appBar: AppBar(
                backgroundColor: Color(0xFF6788FF),
                title: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                        text(
                            'Pause',
                            family: "AllertaStencil",
                            size: 22,
                            weight: FontWeight.w400
                        ),
                        text(
                            'Projekt: 1298721398',
                            family: "Mulish",
                            size: 12,
                            weight: FontWeight.w600
                        ),
                    ],
                ),
            ),
            body: Container(
                height: 575,
                color: kBackgroundColor,
                padding: EdgeInsets.all(kDefaultSpacing),
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                        text(
                            'Kategories',
                            family: "AllertaStencil",
                            size: 22,
                            weight: FontWeight.w400
                        ),
                        Container(
                            decoration: BoxDecoration(border: 
                                Border(
                                    bottom: BorderSide(
                                        color: kSecondaryColor,
                                    )
                                )
                            ),
                            height: 57,
                            width: double.infinity,
                            child: Row(
                                // mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                    Expanded(
                                        child: DropdownButton(
                                            // icon: (Image.asset('assets/icons/drop_down_arrow.png')),
                                            iconSize: 0,
                                            hint: text(
                                                'Wahlen Sie bitte Kategorie aus',
                                                family: 'Mulish',
                                                size: 14,
                                                weight: FontWeight.w600,
                                                color: Colors.grey[350]!
                                            ),
                                            value: _categoryButton1,
                                            onChanged: (String? newValue){
                                                setState(() {
                                                    _categoryButton1 = newValue!;
                                                });
                                            },
                                            items: _categoryItemsMap
                                        )
                                    ),
                                    Image.asset('assets/icons/drop_down_arrow.png'),
                                    SizedBox(width: kDefaultSpacing/2,)
                                ],
                            ),
                        ),
                        SizedBox(height: kDefaultSpacing,),
                        text(
                            'Pause',
                            family: "AllertaStencil",
                            size: 22,
                            weight: FontWeight.w400
                        ),
                        ScrollList(),
                        SizedBox(height:kDefaultSpacing),
                        CommentField(),
                        SizedBox(height:kDefaultSpacing),
                        Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                                text(
                                    'Abbrechen',
                                    family: 'Roboto',
                                    size: 14,
                                    weight: FontWeight.w500
                                ),
                                SizedBox(width: 25,),
                                blackButton(
                                    text: 'Speichern', 
                                    icon: 'assets/icons/send_klein.png', 
                                    height: 42,
                                    width: 127, 
                                    onPress: (){}
                                ),
    
                            ],
                        )
                    ],
                ),
            ),
        );
    }
}