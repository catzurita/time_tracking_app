import 'package:flutter/material.dart';

import './components/days_icons.dart';
import './components/app_bar.dart';
import './components/time_table.dart';
import '../../../constants.dart';
import '../../../widgets/app_drawer.dart';

class TimeTrackingScreen extends StatelessWidget {
    var scaffoldKey = GlobalKey<ScaffoldState>();
    @override
    Widget build(BuildContext context) {
        return Scaffold(
            key: scaffoldKey,
            appBar: appBarBuild(
                scaffoldKey, 
                context
            ),
            drawer: AppDrawer(),
            body: Column(
                children: [
                    Container(
                        height: 75,
                        margin: EdgeInsets.only(top: 0),
                        color: kBackgroundColor,
                        child:  Container(
                            padding: EdgeInsets.only(
                                left: kDefaultSpacing,
                                right: kDefaultSpacing,
                                top: kDefaultSpacing/3
                            ),
                            child:  DaysIcons()
                        )
                    ),
                    Container(
                        child: Expanded(
                            child: SingleChildScrollView(
                                child: TimeTable()
                            ),
                        ),
                    )
                ],
            )
        );
    }
}

