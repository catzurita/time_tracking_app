import 'package:flutter/material.dart';

import '../constants.dart';
Widget blackButton({
    required String text, 
    required String icon, 
    required VoidCallback onPress, 
    required double width, 
    required double height
}){
    return ElevatedButton(
        onPressed: onPress, 
        style: ElevatedButton.styleFrom(
            primary: kPrimaryColor,
            padding: EdgeInsets.symmetric(
                horizontal: 10
            ),
            fixedSize: Size(width, height)
        ),
        child: Row(
            children: [
                Text(
                    text,
                    style: TextStyle(
                        color: kBackgroundColor
                    ),
                ),
                ConstrainedBox(
                    constraints: BoxConstraints(
                        minWidth: 44,
                        minHeight: 44,
                        maxWidth: 64,
                        maxHeight: 64,
                    ),
                    child: Image.asset(icon),
                )
            ],
        )
    );
}