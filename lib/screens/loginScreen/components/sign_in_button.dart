import 'package:flutter/material.dart';

import '../../../constants.dart';
import '../../../widgets/text.dart';

class SignInButton extends StatelessWidget {
   
    @override
    Widget build(BuildContext context) {
        return InkWell(
            onTap: () => Navigator.pushReplacementNamed(
                context, 
                '/my-account',
            ),
            child: Container(
                decoration: BoxDecoration(
                    border: Border(
                        bottom: BorderSide(width: 3.0, color: kSecondaryColor)
                    )
                ),
                margin: EdgeInsets.symmetric(horizontal: 15),
                height: 57,
                child:ListTile(
                    leading: ConstrainedBox(
                        constraints: BoxConstraints(
                            minWidth: 44,
                            minHeight: 44,
                            maxWidth: 64,
                            maxHeight: 64,
                        ),
                        child: Image.asset('assets/images/microsoft.png'),
                    ),
                    title: text(
                        'Sign in with Microsoft',
                        family: 'Mulish',
                        size: 14,
                        weight: FontWeight.w600
                    ),
                    trailing: ConstrainedBox(
                        constraints: BoxConstraints(
                            minWidth: 44,
                            minHeight: 44,
                            maxWidth: 64,
                            maxHeight: 64,
                        ),
                        child: Image.asset('assets/icons/arrow_double_klein.png')
                    ),
                ),
            ),
        );
    }
}