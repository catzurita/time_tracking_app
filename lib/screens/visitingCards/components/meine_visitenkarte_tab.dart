import 'package:flutter/material.dart';

import './kontakt.dart';
import '../../../constants.dart';
import '../../../widgets/text.dart';
import '../../../widgets/person_card.dart';

class MeinVisitenKarteTab extends StatelessWidget {
  
    @override
    Widget build(BuildContext context) {
        return Container(
            width: double.infinity,
            height: 575,
            margin: EdgeInsets.only(
                left: kDefaultSpacing,
                right: kDefaultSpacing,
                top: kDefaultSpacing,
                bottom: kDefaultSpacing/2
            ),
            padding: EdgeInsets.all(kDefaultSpacing),
            color: kBackgroundColor,
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                    text(
                        'Visitenkarte',
                        size: 22,
                        family: "AllertaStencil",
                        weight: FontWeight.w400
                    ),
                    Container(
                        padding: EdgeInsets.only(top: 10),
                        width: double.infinity,
                        child: personCard(
                            name: 'Greg Neu', 
                            email: 'max.mustermann@bkl.de', 
                            position: 'Monteur', 
                            image: 'assets/images/prof-pic.png'
                        )
                    ),
                    SizedBox(height: kDefaultSpacing,),
                    Center(
                        child: Image.asset(
                            'assets/images/qrcode 1.png',
                            height: 244,
                            width: 244,
                        ),
                    ),
                    SizedBox(height: kDefaultSpacing,),
                    text(
                        'Adresse',
                        size: 22,
                        family: "AllertaStencil",
                        weight: FontWeight.w400
                    ),
                    SizedBox(height: kDefaultSpacing/2,),
                    Container(
                        padding: EdgeInsets.only(top: 10),
                        width: double.infinity,
                        child: personCard(
                            name: 'Flutter Bootcamp', 
                            email: '6783 Ayala Avenue', 
                            position: '1200 MetroM Manila', 
                            image: 'assets/icons/adresse.png'
                        )
                    ),
                    SizedBox(height: kDefaultSpacing,),
                    text(
                        'Kontakt',
                        size: 22,
                        family: "AllertaStencil",
                        weight: FontWeight.w400
                    ),
                    SizedBox(height: kDefaultSpacing/2,),
                    Kontakt(
                        t: '+49 1234 56 789 01', 
                        f: '+49 1234 56 789 01-2', 
                        m: '+49 1234 56', 
                        e: 'andreo.mustermann@f-bo'
                    ),
                    SizedBox(height: kDefaultSpacing,),
                    Align(
                        alignment: Alignment.bottomCenter,
                        child: text(
                            'www.flutter-bootcamp.com',
                            family: 'Mulish',
                            size: 12,
                            weight: FontWeight.w600,
                            color: kWebsiteColor
                        ),
                    )
                ],
            ),
        );
            
    }
}