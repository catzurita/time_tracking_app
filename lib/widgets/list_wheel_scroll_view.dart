import 'package:flutter/material.dart';

import '../constants.dart';
import './text.dart';

class ListWheelScroll extends StatefulWidget {
  
    @override
    _ListWheelScrollState createState() => _ListWheelScrollState();
}

class _ListWheelScrollState extends State<ListWheelScroll> {

    // int _selectedItemIndex = 0;

    List generateHoursAndMinutes(){
        List hour = [];
        List minutes = [00,15,30,45];
        for(int i = 0; i < 25; i++){
            for(var minute in minutes){
                if(i == 24){
                    if( minute > 0){
                        break;
                    }
                }
                
                hour.add(
                    ListTile(
                        title: text(
                             i<10 ?
                            '0${i.toString()} : ${minute.toString()} Uhr.' :
                            '${i.toString()} : ${minute.toString()} Uhr.'
                            ,
                            family: 'AllertaStencil',
                            size: 16,
                            weight: FontWeight.w400,
                            // color: _selectedItemIndex == (i + minutes.indexOf(minute)) ? kPrimaryColor : kSecondaryColor
                            color: kPrimaryColor
                        ),
                    )
                );
            }
        }
        return hour;
    }
    
    @override
    Widget build(BuildContext context) {
        return Expanded(
            child:ListWheelScrollView(
                // onSelectedItemChanged: (index){
                //     setState(() {
                //         _selectedItemIndex = index;
                //     });
                // },
                itemExtent: 50,
                useMagnifier: true,
                physics: FixedExtentScrollPhysics(),
                diameterRatio: 3,
                squeeze: 2.0,
                perspective: 0.01,
                children: [...generateHoursAndMinutes()],
            )
        );
    }
}