import 'package:flutter/material.dart';

Widget appbarIcon(Function()? openDrawer){
    return IconButton(
        onPressed: (){
            openDrawer;
        },
        icon: Image.asset(
            'assets/icons/menu.png',
            fit: BoxFit.cover,
        )
    );
}