import 'package:flutter/material.dart';

import '../constants.dart';

Widget text(
    String text, 
    {String? family,  
    double? size, 
    FontWeight? weight, 
    Color color = kPrimaryColor}
){
    return Text(
        text,
        style: TextStyle(
            fontFamily: family,
            fontSize: size,
            fontWeight: weight,
            color: color
        )
    );
}